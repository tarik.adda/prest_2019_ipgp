//Inclusion des modules
const express = require('express');
const path = require('path');
const exphbls = require('express-handlebars');
const session = require('express-session');

var bodyParser = require('body-parser');

const mongoose = require('mongoose');
const helpers = require('handlebars-helpers')();


//Inclusion des variables d'environnement défini dans .env dans la racine
require('dotenv').config();


//Inclusion de l'arborescence du serveur
const acceuil = require('./routes/index');
const verification = require('./routes/verification');
const maintenance = require ('./routes/maintenance');


//Lancement du franmework
const app = express();
var server = require('http').Server(app);


global.io = require('socket.io')(server);


//Initialisation BDD
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://127.0.0.1/raspiquenique', {
	//useMongoClient: true
});
var db = mongoose.connection;



//Websocket lié a la maintenance
io.on('connection',function(socket,req){
	console.log('connection');
	require('./routes/maintenance.js').funcAffHisto(socket);
	require('./routes/maintenance.js').funcBizarre(socket);
	require('./routes/maintenance.js').func(socket);
	require('./routes/maintenance.js').func1(socket);
	require('./routes/maintenance.js').func2(socket);
	require('./routes/maintenance.js').func3(socket);
	require('./routes/verification.js').verif(socket);
	require('./routes/index.js').autotest(socket);

//	socket.on('disconnect',function(socket){
//		console.log("deconnection");
//	});

	return io;
});


/*
//On passe socket au router
app.use(function(req,res,next){
	req.io=io;
	next();
});
*/

app.use(session ({
	secret: 'Les pates ca a jamais ete mon truc',
	resave: false,
	saveUninitialized: true,
	}
));


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Chargement du port depuis dotenv
app.set('port', (process.env.PORT || 3000));

//Configuration du framework, vues et médias public
app.set('views', path.join(__dirname,'views'));
app.engine('handlebars', exphbls({defaultLayout:'layout'}));
app.set('view engine','handlebars');

app.use(express.static(path.join(__dirname,'public')));


//Implémentation de l'arborescence du serveur
app.use('/',acceuil);
app.use('/verification',verification);
app.use('/maintenance',maintenance);

app.get('*', function(req,res) {
	res.send('404 mais bien tenté');
});


//Lancement du serveur
server.listen(app.get('port'), function(){
	console.log('Serveur démarré sur le port '+app.get('port'));
});


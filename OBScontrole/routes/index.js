//Route dédié à l'Acceuil

var serialport = require('serialport')
var express = require('express');
var router = express.Router();
var fs = require('fs');
var spawn = require("child_process").execFileSync;
var rl;
var tampon_test={};

const Readline = require('@serialport/parser-readline');

//Chemin d'acces a l'obs
var path;
var actif={};

var parse;

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

//Permet de savoir si l'OBS est en place ou pas
//Valeure transmise à la vue

//Lancement du process pour le PPS
var child;

var compOBS;
//var Readline = serialport.parsers.Readline;

router.get('/',function(req,res){
	console.log("reinitialise de actif")
	actif={};
	console.log("Acceuil");
	console.log("ID client : "+req.sessionID);

	//Check si l'obs ou la liaison série est branché
	if(fs.existsSync('/dev/OBS')){
		console.log("OBS EXISTE");
		actif.obs=1;
		path='/dev/OBS';
	}
	//Si liason serie
	else if(fs.existsSync('/dev/OBS_serial')){
		console.log("OBS SERIAL EXISTE");
		actif.obs=0;
		path='/dev/OBS_serial';
	}
	//Si rien
	else {
	//	actif.etat=0;
	//	actif.branche=0;
	//	res.render('index', {data: actif});
	//	return;
		path=null
		console.log("Aucun OBS de branche (aucun fichier dev trouve)");
	}

	//On recupere le path pour savoir sur quel OBS on est
	module.exports.path = path;
	console.log(path);
	if(path!=null){
		compOBS = new serialport(path,{baudRate: 9600},function(err){
			if(err){
				//S'il n'y'a pas de convertisseur serie
				actif.branche=0;
				actif.etat=0;
				console.log("Erreur connection OBS");
			}
			else {
				//Si le convertisseur serie est branche
				actif.branche=0;
				actif.etat=1;
				console.log("Connection OBS réussit");
			}
		});
		//Si l'OBS est actif
		if(typeof compOBS !== 'undefined') {
			console.log("connection série initialisé");

			parse = compOBS.pipe(new Readline({delimiter: '\n'}));


			parse.on('data',function(data){
				console.log("recuperation d'une phrase");
				console.log(data);
				console.log(JSON.stringify(data));
			});

			//parser = new Readline({delimiter: '\r'});
			//compOBS.pipe(parser);
			//A la reception de données
			compOBS.on('data', function(data){
				actif.branche=1;
			//	console.log("On recoit des trucs");
			//	console.log(data);
				if(actif.obs == 1){
					actif.identifiant=data[0].toString(16)+data[1].toString(16)+data[2].toString(16)+data[3].toString(16);
					actif.data=data.toString().substring(5);
				}
				//Si l'OBS est bien présent
			});

			//A l'ouverture de la connexion série
			compOBS.on('open', function(){
				console.log("Ouverture OBS. Envois de données");
				compOBS.write('\r');
				console.log("fermeture");
				//Attente jusqu'a reception de rep
				setTimeout(function(){compOBS.close()},100);
			});

		}
		//Attente jusqu'a reception d'une reponse
	}

	child = spawn('/home/pi/TravailTestPPS/gpio_test_PPS', [], {});

	console.log(child.toString());

	if(child.toString().charAt(0)==1){
		console.log("Attention Pas de PPS");
		actif.date = "Aucun signal PPS n'a été détecté";
		//console.log(error);
	}
	else {

		actif.date = new Date();
	}
	console.log("data a envoyer au render");
	console.log(actif);
	setTimeout(function(){res.render('index', {data: actif})},150);
});

module.exports = router;
module.exports.autotest = function(socket){
	socket.on('auto_test',function(data){
		console.log("Procedure d'autotest lancé");

		tampon_test.sys_obs="RIEN NE MARCHE DANS L'OBS";
		tampon_test.sys_capteur="RIEN NE MARCHE DANS LE CAPTEUR";
		tampon_test.sys_adc="RIEN NE MARCHE DANS L'ADC";
		tampon_test.sys_horloge="RIEN NE MARCHE DANS L'HORLOGE";
		tampon_test.sys_memoire="RIEN NE MARCHE DANS LA MEMOIRE";
		tampon_test.sys_conso="RIEN NE MARCHE DANS LA CONSO";

		socket.emit("retour_auto_test",tampon_test);
	});
}

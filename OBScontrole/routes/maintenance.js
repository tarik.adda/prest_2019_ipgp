//Route dédié à la maintenance de l'appareil et de son utilisation

var serial = require('serialport');
var express = require('express');
var router = express.Router();
const { StringDecoder } = require('string_decoder');
const decoder = new StringDecoder('utf-8');
var toBuffer = require('blob-to-buffer');
var mod = require('../models/historique.js');

var path = require('./index.js');

const Readline = require('@serialport/parser-readline')


var compa;
var parsi;

var donnees_BDD={};
//var tmp_BDD=[];

//Sert a la reception de datas de l'OBS
var tmp="";


//Creation de value dans la BDD
var dataInfo={};
//SessionID
var session;

//Permet de recuperer les values de la BDD pour les afficher dans l'historique
var tmpArr=[];
var sessArr=[];
var dateArr=[];

var i=0;

//Promises retourné par les modeles, sert a la recuperation de TOUTES les datas en synchro
var promiseALL=[];

//Page d'acceuil
router.get('/',function(req,res){
	console.log("Maintenance");
	session = req.sessionID;
	res.render('maintenance');
});

//Page historique
router.get('/historique',function(req,res){
	console.log("Demande d'historique");
	dateArr=[];
	sessArr=[];
	donnees_BDD={};

 	mod.getInfoSession().then(function(doc) {
		//Recuperation des sessions
		//console.log(JSON.parse(doc)+ "test du type");
		//console.log(JSON.parse(doc)+ "test type");
		console.log(doc[0].object[0]);
		console.log(typeof(doc[0]));
		sessArr=doc[0].object;
//		console.log("KAROAKR"+sessArr[0])
		console.log("recupération des session: "+JSON.stringify(sessArr));
		for(i=0;i<sessArr.length;i++){
			console.log("recuperation des datas des session pour: "+sessArr[i]);
			promiseALL[i]=mod.getInfoSessID(sessArr[i]);
/*.then(function(docu){
	//			sessArr[i].date=docu.date;
	//			console.log("lalaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+sessArr[i]);
				//C'est la que j'ai toutes les données d'une sess
				console.log(typeof(docu.date));
//				dateArr[i]=docu.date;
//				console.log(sessArr[i]);
				//Recuperation des dates
				console.log(docu._doc);
//				tmp_BDD[i].push=docu.date;
//				tmp_BDD[i].session=session;
				console.log("Récupération des données d'une sess: "+dateArr+"  "+tmpArr);
			}));
*/
		}
		Promise.all(promiseALL).then(function(values){
			console.log(values.length);
			for(i=0;i<values.length;i++){
				//console.log("Initialisation des tab a afficher: "+
				dateArr[i]=values[i].date.toISOString();
				//console.log("Initialisation des tab a afficher: "+
				tmpArr[i]=values[i].session;
				console.log(values[i].session);
				console.log(values[i].date);
			}
//			console.log(dateArr);
//			console.log(tmpArr);
			values=[];
			promiseAll=[];
			sessArr=[];
		//	dateArr=[];
		//	tmpArr=[];
		//	values=[];
			donnees_BDD.date = dateArr;
			donnees_BDD.sess = tmpArr;

			console.log('FINAL DATA : '+donnees_BDD.date[0]);
//JSON.stringify(donnees_BDD));
			res.render('historique', {data: donnees_BDD});

		});;

	});
});



module.exports = router;

//Retour des datas de la BDD correspondant à la session
module.exports.funcAffHisto = function(socket){
	socket.on('affiche_histo',function(msgs){
		console.log('historique détaillé: '+msgs);
	 	mod.getInfoSessIDs(msgs).then(function(docs) {
			console.log("Donnees recu de la BDD: "+docs);
			socket.emit("affiche_histo_recu",docs);
		});
//		socket.emit('datas',donnees_BDD);

	});
}
//JS c'est vraiment de la daube alors on fait avec ce qu'on a
module.exports.funcBizarre = function(socket){
	socket.on('historique',function(){
/*		var output=tmpArr.map(function(obj,index){
			var myobj={[index]};
			myobj[dateArr[index]] = obj;
			return myobj;
		});

//		var outputAsc = new Map([...output.entries()].sort())
		output.sort(function(a,b){
			var c = new Date(a.date);
			var d = new Date(b.date);
			return c-d;	
		});

		console.log(output[0]);
//		console.log(outputAsc);
*/
		console.log(JSON.stringify(dateArr));
		donnees_BDD.date = dateArr;
		donnees_BDD.sess = tmpArr;
		console.log('Affichage historique simple '+JSON.stringify(donnees_BDD));

		socket.emit('datas',donnees_BDD);
		donnees_BDD={};
		dateArr=[];
		tmpArr=[];
	});
}

//Fonction gerant la communication direct avec le port serie de l'OBS soit l'écriture
module.exports.func = function(socket){
	socket.on('commande',function(msg){
		console.log('commande : '+msg);
		compa.write(msg,function(){
			console.log("Save dans la BDD"+msg);
			mod.createInfo(dataInfo,session,"commande",msg);
		});
		compa.write('\r');
	});
}

//Fonction démarrant une communication avec l'OBS
module.exports.func1 = function(socket){
	socket.on('start',function(){
		console.log('creation de la liaison serie');
		console.log(path.path);
		compa = new serial(path.path, {baudRate: 9600}, function(err){
			if(err){
				actif.etat=0;
				res.redirect('/');
				console.log("convertisseur serie non branché");
			} else{
				console.log("connection série initialisé");
			}
		});
		console.log("configuration de la liaison série");
//		parsi = compa.pipe(new Readline({ delimiter: '\n' }));
		parsi = compa.pipe(new Readline());
//		parso = new readline({delimiter: '?'});

		console.log("configuration des events");
		compa.on('close',function(){
			console.log("fermeture");
		});

		compa.on('data',function(data){
//			console.log("reception de data: \n\n\n\n\n");
//			console.log("reception de data: "+data.toString('utf8'));
//			console.log("reception de data fin");
			console.log(JSON.stringify(data.toString()));

			console.log(data);
			io.emit('retour', data.toString());
		});

		compa.on('write',function(){
			console.log("ecriture");	

		});

		compa.on('error',function(){
			console.log("erreur liaison serie");
		});

		parsi.on('data',function(dataz){
			console.log("Phrase recu");
			console.log(JSON.stringify(dataz.toString()));
			//tmp+=decoder.write(data);
		//	tmp+=data.toString('utf-8');
		//	console.log(typeof(tmp)+"  "+tmp);
		//	console.log("data recu : "+tmp.indexOf('\r')+" et "+tmp.includes('?'));

		//	if(tmp.includes('\r') ){
//|| tmp.includes('?')) {
//			mod.createInfo(dataInfo,session,"retour",data);
//			console.log("Trame entiere recu:  "+data.toString('utf8'));
//			console.log("Trame entiere recu:  "+data.toString('utf8'));
//			console.log("Trame entiere recu:  "+data.toString('utf8'));
//			console.log("Trame entiere recu:  "+data.toString('utf8'));
//			console.log("Trame entiere recu:  "+data.toString('utf8'));
//			console.log("Trame entiere recu:  "+data.toString('utf8'));


//			io.emit('retour', dataz);


//			tmp="";
//					console.log("Buffer vidé? : "+tmp);
		 //	}else{
//				console.log("Le Buffer pas pret Pas encore");
		//	}

		});


/*		parsi.on('data', function(data){
			console.log("données recu retour d'info "+data);
			io.emit('retour', data);
		});
*/

		compa.on('open',function(){
			console.log("ouverture");
			//compa.write('\r')
		});
	});
}
//Fonction permettant de rafraichir l'OBS
module.exports.func2 = function(socket){
	socket.on('refresh',function(){
		console.log('rafraichissement ');
		compa.write('\r');
	});
}
//Fonction permettant de gerer la fermeture de l'OBS
module.exports.func3 = function(socket){
	socket.on('end',function(msg){
		console.log('end');
		compa.close();
	});
}


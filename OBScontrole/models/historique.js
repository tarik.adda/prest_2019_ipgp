var mongoose = require('mongoose');

var tmp;
var InfoSchema = mongoose.Schema({
	idord: {
		type: Number,
		index: true
	},
	session: {
		type: String
	},
	ordres: {
		type: String
	},
	infos: {
		type: String
	},
	date: {
		type: Date
		}
},{strict: false});

var Info = module.exports = mongoose.model('Info',InfoSchema);

//Creer entrées dans BDD
module.exports.createInfo = function(newInfo,sesID,ordre,info){
	console.log("creation info ");
	newInfo.ordres = ordre;
	newInfo.infos = info;
	newInfo.session = sesID;

	newInfo.date = new Date().toISOString();

	var newI = Info(newInfo);
	newI.save(function(err){
		if(err) console.log(err);
	});
	console.log(newInfo.date);
};

//Recupere toutes les data
module.exports.getInfos = function() {
	console.log("Demande de toutes les datas");
	return Info.find(function(err,tmp) {
		if (err) {
			console.error(err);
		} else {
			console.log("réussite: getInfos ");
		}
	}).sort('date').populate('tempo').exec();
}

//Recupere les sessions existantes par ordre d'entrées croissantes
module.exports.getInfoSession = function() {
	console.log("Demande des sessions");
 //	Info.find({}).sort('date').populate('tmp').exec(function(err,tmp){
//			return tmp;
return Info.aggregate([
{$match :{}},
{$group : {
	_id : '$session',
	}
},
{$sort: {"date":1}},
{$group : {
	_id :null,
	'object': {$push:'$_id'}
	}
},
{$project: {_id:0,'object':'$object'}
}
]
//,function(err,result){
//if(err){
//console.log(err)
//}else{
//return result;
//console.log(result);
//}
//})
).exec();

//return Info.distinct('session').exec();
//		});
}

//Recuperation d'une pour une session
module.exports.getInfoSessID = function(idSess) {
	console.log("Demande ds données pour l'id: "+idSess);
	var query = {
			session: idSess
			};
	return Info.findOne(query).populate('tempo').exec(function(err,tmp) {
		if (err) {
			console.error(err);
		} else {
			console.log("réussite: getInfoSessID ");
		}
	});
}

//Trouver toutes les datas a partir d'une session
module.exports.getInfoSessIDs = function(idSess) {
	console.log("Demande des données pour l'id: "+idSess);
	var query = {session: idSess};
	return Info.find(query, function(err,tmp) {
		if (err) {
			console.error(err);
		} else {
			console.log("réussite: getInfoSessIDs");
		}
	}).populate('tempo').exec();
}

/*
var i;
var tmp=[];
//Retourne les promess dans l'odre etabli dans le tab
module.exports.getInfoSessIDTAB = function(idSes) {

	for(i=0;i<idSes.length;i++){
		query={
			session: idSes[i];
		};


	}
	console.log("Demande ds données pour l'id: "+idSess);
	var query = {
			session: idSess
			};
	return Info.findOne(query).populate('tempo').exec(function(err,tmp) {
		if (err) {
			console.error(err);
		} else {
			console.log("réussite: getInfoSessID ");
		}
	});
}
*/

/********************************************************
	TEST DU PPS
*********************************************************/


#define _GNU_SOURCE

#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <time.h>

#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */




/******************
Variables globales 
*******************/

/************************
Prototypes des fonctions 
************************/

long long int get_my_nanotime();


/**********
*** MAIN **
***********/

int main (int argc, char *argv[]){
	//Variable pour les tests des pins
	int prec_val_test;
	int cur_val_test;


	long long int time_flag;
	long long int time_val;

	//Initialisaiton GPIO
	wiringPiSetupGpio();
	pinMode(23,INPUT);

	//DEBUT du test des pin pour verifier qu'on a bien les PPS sur chacun
	//Initialisation de la lecture
	cur_val_test=digitalRead(23);
	time_flag = get_my_nanotime();

	//Lecture jusqu'a l'obtention d'un front montant

	do{

		prec_val_test=cur_val_test;
		cur_val_test=digitalRead(23);

		time_val = llabs(get_my_nanotime()-time_flag);
	}while(!(cur_val_test==1 && prec_val_test==0) && time_val <= 2000000000 );

	if (time_val > 2000000000 ){
		printf("1 : AUCUN SIGNAL DETECTE : pin %d \n",23);
	}
	else {
		printf("0 : PPS DETECTE \n");
	}
	return 0;
}

/****************************************************************************
Procédure de calcul de temps
  retourne une unité temporelle en nanoseconde à l'instant ou elle est appelé
*****************************************************************************/

/* Recupération des temps d'execution*/
long long int get_my_nanotime()
{
    /* Structure pour récupérer le temps en sec. et nanosec. */
    struct timespec toto ;
    struct tm titi ;

    /* Temp. pour les calculs */
    long long int nb_sec ;
    long long int nb_nsec ;

    /* Récupération du temps sur l'horloge CLOCK_REALTIME */
    if (clock_gettime(CLOCK_REALTIME,&toto) != 0)
    {
      printf("if( clock_gettime(CLOCK_REALTIME,&toto) != 0)\r\n");
      exit(1);
    }
    /* Conversion de la valeur récupéré (il s'agissait d'une date) */
    gmtime_r(&(toto.tv_sec),&titi);
    /* Calcul du temps en  nanosecondes*/
    nb_sec = titi.tm_sec + 60 * (titi.tm_min + 60 * (titi.tm_hour /*+ 24 * titi.tm_mday)*/));
    nb_nsec = nb_sec * 1000000000L + toto.tv_nsec ;
    return nb_nsec;
}


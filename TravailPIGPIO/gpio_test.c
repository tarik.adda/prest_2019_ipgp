/********************************************************
	DERIVE ENTRE DEUX SIGNAUX

*  Fournit le temps (a 20 us prés) entre deux réceptions 
*  de fronts montants
*  Entrées :
*  c1 : Coeur sur lequel executer la lecture du premier PIN
*  c2 : Coeur sur lequel executer la lecture de la seconde PIN
*  p : Priorité d'execution pour le scheduler (0 - 99)
*  o : Entrée du signal à comparer

* Les copyright sont a tarik Adda-Benatia

*********************************************************/


#define _GNU_SOURCE

#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <time.h>

#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */

#include <pthread.h>

#include <sched.h>
#include <semaphore.h>

/******************
Variables globales 
*******************/
#define NB_ACQ 10
//Parametres thread
struct argument {
	int a;
	long long int ret[NB_ACQ];
};

sem_t semaphore;
//Priorité des thread pour le scheduler
int prio=99;

/************************
Prototypes des fonctions 
************************/

long long int get_my_nanotime();
void *Thread_Lect(void *pin); 


/**********
*** MAIN **
***********/

int main (int argc, char *argv[]){
	//Variable pour les tests des pins
	int prec_val_test;
	int cur_val_test;
	long long int time_flag;
	long long int time_val;

	long long int result[NB_ACQ];

	sem_init(&semaphore,0,1);

	//Prepration pour avoir 100% tps CPU pas sur qu'on ait besoin
	//system("echo -1 >/proc/sys/kernel/sched_rt_runtime_us");

	//Ecriture du temps dans un fichier
	FILE *f1, *fopen();

	//Cpu qu'on choisit pour les calculs
	cpu_set_t cpuset;

	//ID Thread
	pthread_t thread_lect_pin1;
	pthread_t thread_lect_pin2;

	//Arguments Thread
	struct argument *A = (struct argument *)malloc(sizeof(struct argument));
	struct argument *B = (struct argument *)malloc(sizeof(struct argument));

	//Status Thread
	void *status1;
	void *status2;

	//CPU Thread
	int num_cpu1=0;
	int num_cpu2=3;

	//PIN a tester
	int pps_obs=11;

	int i =0;
	//Recuperation des arguments
	if( argc > 1) {
		while(i<(argc-1)){
			if(strcmp("-c1", argv[i+1]) == 0){
				num_cpu1 = strtol(argv[i+2],0,0);
				printf("Modif CPU : %d \n",num_cpu1);
			}

			if(strcmp("-c2", argv[i+1]) == 0){
				num_cpu2 = strtol(argv[i+2],0,0);
				printf("Modif CPU : %d \n",num_cpu2);
			}



			if(strcmp("-p", argv[i+1]) == 0){
				prio = strtol(argv[i+2],0,0);
				printf("Modif prio : %d \n",prio);
			}


 			if(strcmp("-o", argv[i+1]) == 0){
				pps_obs = strtol(argv[i+2],0,0);
				printf("Modif pin pps : %d \n",pps_obs);
			}

			i=i+2;
		}
	}

	//Initialisation, 1ere value = pin a lire 2eme retour
	A->a = 23;
	for(int x=0;x<NB_ACQ;x++){
		A->ret[x] = 0;
	}

	B->a = pps_obs;
	for(int x=0;x<NB_ACQ;x++){
		B->ret[x] = 0;
	}

	//Initialisaiton GPIO
	wiringPiSetupGpio();
	pinMode(11,INPUT);
	pinMode(23,INPUT);
	//DEBUT du test des pin pour verifier qu'on a bien les PPS sur chacun
	//Initialisation de la lecture
	cur_val_test=digitalRead(A->a);
	time_flag = get_my_nanotime();

	//Lecture jusqu'a l'obtention d'un front montant
	do{
		prec_val_test=cur_val_test;
		cur_val_test=digitalRead(A->a);

		time_val = llabs(get_my_nanotime()-time_flag);

	} while(!(cur_val_test==1 && prec_val_test==0) && time_val <= 2000000000 );

	if (time_val > 2000000000 ){
		printf("AUCUN SIGNAL DETECTE : pin %d \n",A->a);
		return 1;
	}

	printf("Obtention d'un front montant valide : %lld  sur : %d  \n",time_val,A->a);

	//Initialisation de la lecture
	cur_val_test=digitalRead(B->a);
	time_flag = get_my_nanotime();

	//Lecture jusqu'a l'obtention d'un front montant
	do{
		prec_val_test=cur_val_test;
		cur_val_test=digitalRead(B->a);

		time_val = llabs(get_my_nanotime()-time_flag);

	} while(!(cur_val_test==1 && prec_val_test==0) && time_val <= 2000000000 );

	if (time_val > 2000000000 ){
		printf("AUCUN SIGNAL DETECTE : pin %d \n",B->a);
		return 1;
	}

	printf("Obtention d'un front montant valide : %lld  sur : %d  \n",time_val,B->a);


	//FIN des tests des pins
	/* Création thread1 pour lect pin */
	if((pthread_create(&thread_lect_pin1,NULL,Thread_Lect,(void *)A))!=0){
		perror("bind fail");
		return 1;
	}


	/* Création thread1 pour lect pin */
	if((pthread_create(&thread_lect_pin2,NULL,Thread_Lect,(void *)B))!=0){
		perror("bind fail");
		return 1;
	}


	//On leur attribue un coeur cpu a chacun
        CPU_ZERO(&cpuset);
        CPU_SET(num_cpu1, &cpuset);


	if((pthread_setaffinity_np(thread_lect_pin1, sizeof(cpu_set_t), &cpuset))!=0){
		printf("pthread_setaffinity_np error\n\r");
	}

        CPU_ZERO(&cpuset);
        CPU_SET(num_cpu2, &cpuset);


	if((pthread_setaffinity_np(thread_lect_pin2, sizeof(cpu_set_t), &cpuset))!=0){
		printf("pthread_setaffinity_np error\n\r");
	}


	//Attente fin d'execution des threads
	pthread_join(thread_lect_pin1,&status1);
	pthread_join(thread_lect_pin2,&status2);
/*
	for(int x=0;x<50;x++){
		printf("val: %lld \n",A->ret[x]);
		printf("val: %lld \n",B->ret[x]);
	}
*/


	int offset=0;
	f1=fopen("test.txt","w");

	for(int x=1;x<NB_ACQ;x++){
		fprintf(f1,"A[%d] - A[%d] = %lld  B[%d] - B[%d] = %lld \n",x,x-1,A->ret[x]-A->ret[x-1],x,x-1,B->ret[x]-B->ret[x-1]);
		//Si l'ecart est au dessus du seuil on depile le tab en face 
		if(B->ret[x]-B->ret[x-1] > 1500000000 ){
			fprintf(f1,"décalage lancé \n");
			offset++;
			for(int z=x;z<NB_ACQ;z++){
				fprintf(f1,"remplacement de l'element %d par z+1 \n",z);
				A->ret[z]=A->ret[z+1];
			}
			fprintf(f1,"décalage fini \n");

		} else if(B->ret[x]-B->ret[x-1] < 6500000){
			fprintf(f1,"décalage lancé \n");
			offset++;
			for(int z=x;z<NB_ACQ;z++){
				fprintf(f1,"remplacement de l'element %d par z+1 \n",z);
				B->ret[z]=B->ret[z+1];
			}
			fprintf(f1,"décalage fini \n");
		}

	}


	fprintf(f1,"\n \n BOUCLE DE VERIFICATION \n \n");

	for(int x=NB_ACQ-offset;x>=0;x--){
		fprintf(f1,"A[%d] - A[%d] = %lld  B[%d] - B[%d] = %lld \n",x,x-1,A->ret[x]-A->ret[x-1],x,x-1,B->ret[x]-B->ret[x-1]);
	}

	fprintf(f1,"\n \n CALCUL DES VALEURS \n \n");

	for(int x=0;x<NB_ACQ-offset;x++){
		fprintf(f1,"A[%d] = %lld B[%d] = %lld \n",x,A->ret[x],x,B->ret[x]);
		result[x]=llabs(A->ret[x]-B->ret[x]);
	}


	long long int resultat=0;


	for(int x=0;x<NB_ACQ-offset;x++){
		resultat=resultat+result[x];
		fprintf(f1,"%lld \n",result[x]);
	}


	if (resultat > 1000000000){
		resultat = 1000000000-resultat;
	}

	fclose(f1);

	resultat = resultat/(NB_ACQ-offset);

	printf("\n \n RESULTAT \n \n");


	printf("Temps d'écart:  %lld nanosecondes ou %lld \n",resultat,1000000000-resultat);
	printf("Temps d'écart:  %f secondes \n",(float)(resultat)/1000000000);

//	sem_destroy(&semaphore);

	//Ecriture dans un fichier texte pour le fun
	return(0);

}

void * Thread_Lect(void *pin) {

	struct sched_param my_sched_param;
	//Valeur precedente pour detecter le rising edge
	int prec_val;
	//Valeur courante pour detecter le rising edge
	int cur_val;

	//Recuperation des arguments
	int test = ((struct argument*)pin)->a;

	printf("Debut de lecture pour %d \n",test);

	//Set la priorité pour le scheduler
	my_sched_param.sched_priority = prio;
	sched_setscheduler(0,SCHED_RR, &my_sched_param);


	for(int x=0;x<NB_ACQ;x++){
		cur_val = 0;
		prec_val=0;
		//Initialisation de la lecture
		cur_val=digitalRead(test);
		//Lecture jusqu'a l'obtention d'un front montant
		//sem_wait(&semaphore);
		//section critique
		do{
			prec_val=cur_val;
			cur_val=digitalRead(test);

		} while(!(cur_val==1 && prec_val==0));
		((struct argument*)pin)->ret[x] = get_my_nanotime();
		//sem_post(&semaphore);

		printf("Fin d'obtention du front montant a : %lld sec. sur  %d num: %d \n",get_my_nanotime(),test,x);
	}
	pthread_exit(NULL);
	
}


/****************************************************************************
Procédure de calcul de temps
  retourne une unité temporelle en nanoseconde à l'instant ou elle est appelé
*****************************************************************************/

/* Recupération des temps d'execution*/
long long int get_my_nanotime()
{
    /* Structure pour récupérer le temps en sec. et nanosec. */
    struct timespec toto ;
    struct tm titi ;
    
    /* Temp. pour les calculs */
    long long int nb_sec ;
    long long int nb_nsec ;
    
    /* Récupération du temps sur l'horloge CLOCK_REALTIME */
    if (clock_gettime(CLOCK_REALTIME,&toto) != 0)
    {
      printf("if( clock_gettime(CLOCK_REALTIME,&toto) != 0)\r\n");
      exit(1);
    }
    /* Conversion de la valeur récupéré (il s'agissait d'une date) */
    gmtime_r(&(toto.tv_sec),&titi);
    /* Calcul du temps en  nanosecondes*/
    nb_sec = titi.tm_sec + 60 * (titi.tm_min + 60 * (titi.tm_hour /*+ 24 * titi.tm_mday)*/));
    nb_nsec = nb_sec * 1000000000L + toto.tv_nsec ;
    return nb_nsec;
}


import time
import board
import adafruit_ina260

# Connexion au  INA260
i2c = board.I2C()
ina260 = adafruit_ina260.INA260(i2c)

compteur = 0

# Calcul des moyennes
moyInten = 0
moyTens = 0
moyPow = 0

# Declaration d'un tableau a 2 dimensions 11 listes de 3 item
res = [[0 for x in range(3)] for y in range(11)]

while compteur < 10 :
  print("Courant: %.2f mA Voltage: %.2f V Puissance: %.2f mW"
        %(ina260.current, ina260.voltage, ina260.power))
#  print("Voltage: %f V"
#        %(ina260.voltage))

  res[compteur][0] = ina260.current
  res[compteur][1] = ina260.voltage
  res[compteur][2] = ina260.power

  moyInten = moyInten + ina260.current
  moyTens = moyTens + ina260.voltage
  moyPow = moyPow + ina260.power

  compteur = compteur +1
  time.sleep(0.2)


res[10][0] = moyInten/10
res[10][1] = moyTens/10
res[10][2] = moyPow/10

print("Valeur a transmettre, toutes les valeurs avec les moyennes en derniere position (10eme): ",res)
